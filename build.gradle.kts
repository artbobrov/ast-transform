plugins {
    val kotlinVersion = "1.4.0"
    java
    application
    kotlin("jvm") version kotlinVersion
}

group = "org.example"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven(url = "https://kotlin.bintray.com/kotlinx")
}


application {
    mainClassName = "MainKt"
}

dependencies {
    implementation("org.jetbrains.kotlinx:kotlinx-cli:0.3")

    testImplementation("org.junit.jupiter", "junit-jupiter", "5.7.0")
}


tasks {
    test {
        useJUnitPlatform()
    }
}