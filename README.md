# Test task for AST Transformations for the Coding Assistant project 

## Usage

Написан cli интерфейс с помощью [kotlinx-cli](https://github.com/Kotlin/kotlinx-cli).

```bash
λ ./gradlew run --args="--help"

> Task :run
Usage: brackets options_list
Arguments:
    input -> Input string { String }
Options:
    --brackets, -b [PARENTHESES] -> Brackets selection strategy { Value should be one of [parentheses, brackets, braces, circled] }
    --symbols [(){}[]] -> Symbols to use in circled mode { String }
    --surrond, -s [false] -> Surround the given string with brackets
    --center, -c [false] -> Insert brackets in the center of the given string (works if string length is even)
    --help, -h -> Usage info


BUILD SUCCESSFUL in 694ms
2 actionable tasks: 1 executed, 1 up-to-date
```

Использование #1:

```bash
λ ./gradlew run --args="-s -b parentheses abcde"

> Task :run
(a(b(c)d)e)

BUILD SUCCESSFUL in 612ms
2 actionable tasks: 1 executed, 1 up-to-date
```

Использование #2:

```bash
λ ./gradlew run --args="-s -c -b circled --symbols [](){}<> abcdef"

> Task :run
[a(b{c<>d}e)f]

BUILD SUCCESSFUL in 619ms
2 actionable tasks: 1 executed, 1 up-to-date
```

Написаны [тесты](./src/test/java/transform/StringIrTransformerTest.kt) на обработку строки.

Алгоритм преобразования работает за O(n).

## Architecture

Базовая логика в преобразования в `DefaultStringIrTransformer`. 
Дополнительное поведение добавляется с помощью паттерна "Декоратор"(добавляет какое-то поведение уже существующему алгоритму).


