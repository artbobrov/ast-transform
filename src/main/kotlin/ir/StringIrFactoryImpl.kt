package ir

import tokenize.Token

class StringIrFactoryImpl : StringIrFactory {
    override fun build(tokens: List<Token>): StringIr {

        var leftIndex = 0
        var rightIndex = tokens.size - 1
        val components: MutableList<StringComponent> = ArrayList<StringComponent>().also {
            it.ensureCapacity(tokens.size / 2 + 1)
        }
        while (leftIndex < rightIndex) {
            val component = StringComponent.Paired(tokens[leftIndex] to tokens[rightIndex])
            components.add(component)
            leftIndex++
            rightIndex--
        }

        if (leftIndex == rightIndex) {
            val component = StringComponent.Single(tokens[leftIndex])
            components.add(component)
        }

        val canInsertInCenter = tokens.size % 2 == 0
        return StringIr(components, canInsertInCenter)
    }
}