package ir

import tokenize.Token


interface StringIrFactory {
    fun build(tokens: List<Token>): StringIr
}