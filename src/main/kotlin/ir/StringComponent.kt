package ir

import tokenize.Token

sealed class StringComponent {
    data class Single(val token: Token) : StringComponent() {
        override val elementsCount: Int get() = 1
    }

    data class Paired(val tokens: Pair<Token, Token>) : StringComponent() {
        override val elementsCount: Int get() = 2
    }

    companion object {
        val PARENTHESES = Paired(Token('(') to Token(')'))

        val BRACKETS = Paired(Token('[') to Token(']'))

        val BRACES = Paired(Token('{') to Token('}'))
    }

    abstract val elementsCount: Int
}