package ir

data class StringIr(
        val components: List<StringComponent>,
        val canInsertInCenter: Boolean
) {
    // Complexity: O(n)
    fun toPrettyString(): String {
        var result = ""
        for (component in components) {
            result += when (component) {
                is StringComponent.Single -> component.token.symbol
                is StringComponent.Paired -> component.tokens.first.symbol
            }
        }
        for (component in components.reversed()) {
            if (component is StringComponent.Paired) {
                result += component.tokens.second.symbol
            }
        }
        return result
    }
}