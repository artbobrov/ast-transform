package cli

import kotlinx.cli.ArgParser
import kotlinx.cli.ArgType
import kotlinx.cli.default
import kotlinx.cli.optional

object CliArguments {
    enum class BracketsType {
        PARENTHESES,
        BRACKETS,
        BRACES,
        CIRCLED
    }

    private const val CLI_NAME = "brackets"
    private val parser = ArgParser(CLI_NAME)

    val brackets by parser.option(
            ArgType.Choice<BracketsType>(),
            shortName = "b",
            description = "Brackets selection strategy"
    ).default(BracketsType.PARENTHESES)

    val input by parser.argument(ArgType.String, description = "Input string")

    val symbols by parser.option(ArgType.String, description = "Symbols to use in circled mode").default("(){}[]")


    val surrond by parser.option(
            ArgType.Boolean,
            shortName = "s",
            description = "Surround the given string with brackets"
    ).default(false)

    val center by parser.option(
            ArgType.Boolean,
            shortName = "c",
            description = "Insert brackets in the center of the given string (works if string length is even)"
    ).default(false)


    fun parse(args: Array<String>) = parser.parse(args)
}