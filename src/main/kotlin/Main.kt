import cli.CliArguments
import ir.StringIrFactory
import ir.StringIrFactoryImpl
import tokenize.Tokenizer
import tokenize.TokenizerImpl
import transform.StringIrTransformFactory


private fun bracketsStrategy(arguments: CliArguments): StringIrTransformFactory.BracketsType {
    return when (arguments.brackets) {
        CliArguments.BracketsType.PARENTHESES -> StringIrTransformFactory.BracketsType.Parentheses
        CliArguments.BracketsType.BRACKETS -> StringIrTransformFactory.BracketsType.Brackets
        CliArguments.BracketsType.BRACES -> StringIrTransformFactory.BracketsType.Braces
        CliArguments.BracketsType.CIRCLED -> {
            if (arguments.symbols.length % 2 != 0)
                throw IllegalArgumentException("Illegal symbols value")
            val pairs = arguments.symbols.windowed(2, step = 2).map { it[0] to it[1] }
            StringIrTransformFactory.BracketsType.Circled(pairs)
        }
    }
}

fun main(args: Array<String>) {
    val tokenizer: Tokenizer = TokenizerImpl()
    val irFactory: StringIrFactory = StringIrFactoryImpl()
    CliArguments.parse(args)


    val irTransformer = StringIrTransformFactory.createTransform(
            bracketsStrategy(CliArguments),
            CliArguments.surrond,
            CliArguments.center
    )

    val tokens = tokenizer.tokenize(CliArguments.input)
    val ir = irFactory.build(tokens)
    val newIr = irTransformer.transform(ir)

    println(newIr.toPrettyString())
}