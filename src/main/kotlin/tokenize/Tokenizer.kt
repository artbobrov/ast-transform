package tokenize

interface Tokenizer {
    fun tokenize(text: String): List<Token>
}