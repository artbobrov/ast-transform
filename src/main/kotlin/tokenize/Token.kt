package tokenize

data class Token(val symbol: Char)