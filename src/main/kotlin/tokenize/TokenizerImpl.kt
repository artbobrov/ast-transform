package tokenize

class TokenizerImpl : Tokenizer {
    override fun tokenize(text: String): List<Token> {
        return text.map { Token(it) }
    }
}