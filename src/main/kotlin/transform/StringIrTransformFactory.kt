package transform

import ir.StringComponent
import tokenize.Token

object StringIrTransformFactory {
    sealed class BracketsType {
        object Parentheses : BracketsType()
        object Brackets : BracketsType()
        object Braces : BracketsType()
        data class Circled(val symbols: List<Pair<Char, Char>>) : BracketsType()
    }

    fun createTransform(bracketsStrategy: BracketsType, surroundString: Boolean, intoCenter: Boolean): StringIrTransformer {
        var transformer: StringIrTransformer = when (bracketsStrategy) {
            BracketsType.Parentheses -> PlainStringIrTranformer(StringComponent.PARENTHESES)
            BracketsType.Brackets -> PlainStringIrTranformer(StringComponent.BRACKETS)
            BracketsType.Braces -> PlainStringIrTranformer(StringComponent.BRACES)
            is BracketsType.Circled -> {
                val components = bracketsStrategy.symbols.map {
                    StringComponent.Paired(Token(it.first) to Token(it.second))
                }
                CircledStringIrTransformer(components)
            }
        }

        if (surroundString)
            transformer = SurroundStringIrTranformer(transformer)

        if (intoCenter)
            transformer = CenterStringIrTransformer(transformer)

        return transformer
    }
}