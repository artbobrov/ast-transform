package transform

import ir.StringComponent

open class PlainStringIrTranformer(private val component: StringComponent) : DefaultStringIrTransformer() {
    override fun nextComponent(): StringComponent? = component
}