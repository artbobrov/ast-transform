package transform

import ir.StringComponent
import ir.StringIr

abstract class DefaultStringIrTransformer : StringIrTransformer {
    // Basing transformation implementation
    // Complexity: O(n)
    override fun transform(ir: StringIr): StringIr {
        val components: MutableList<StringComponent> = ArrayList<StringComponent>().also {
            it.ensureCapacity(ir.components.size * 2)
        }

        for ((offset, component) in ir.components.withIndex()) {
            components.add(component)
            if (offset != ir.components.size - 1) {
                val newComponent = nextComponent() ?: break
                components.add(newComponent)
            }
        }

        return StringIr(components, ir.canInsertInCenter)
    }
}