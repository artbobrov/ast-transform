package transform

import ir.StringComponent
import ir.StringIr

class CenterStringIrTransformer(private val transformer: StringIrTransformer) : StringIrTransformer {
    override fun transform(ir: StringIr): StringIr {
        val newIrComponents = transformer.transform(ir).components.toMutableList()
        if (ir.canInsertInCenter) {
            val nextComponent = transformer.nextComponent() ?: return ir

            newIrComponents.add(nextComponent)
        }
        return StringIr(newIrComponents, ir.canInsertInCenter)
    }

    override fun nextComponent(): StringComponent? = transformer.nextComponent()
}