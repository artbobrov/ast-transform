package transform

import ir.StringComponent


class CircledStringIrTransformer(
        private val circleComponents: List<StringComponent>
) : DefaultStringIrTransformer() {
    private var currentComponentIndex: Int = 0

    override fun nextComponent(): StringComponent? {
        val component = circleComponents.getOrNull(currentComponentIndex)
        currentComponentIndex = (currentComponentIndex + 1) % circleComponents.size
        return component
    }
}