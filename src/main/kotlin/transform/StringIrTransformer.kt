package transform

import ir.StringComponent
import ir.StringIr

interface StringIrTransformer {
    fun transform(ir: StringIr): StringIr

    fun nextComponent(): StringComponent?
}