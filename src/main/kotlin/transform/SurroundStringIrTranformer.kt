package transform

import ir.StringComponent
import ir.StringIr

class SurroundStringIrTranformer(
        private val transformer: StringIrTransformer
) : StringIrTransformer {

    override fun transform(ir: StringIr): StringIr {
        val nextComponent = transformer.nextComponent() ?: return ir
        val newIrComponents = transformer.transform(ir).components.toMutableList()
        newIrComponents.add(0, nextComponent)
        return StringIr(newIrComponents, ir.canInsertInCenter)
    }

    override fun nextComponent(): StringComponent? = transformer.nextComponent()
}