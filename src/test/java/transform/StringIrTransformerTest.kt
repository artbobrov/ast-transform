package transform

import ir.StringIrFactory
import ir.StringIrFactoryImpl
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import tokenize.Tokenizer
import tokenize.TokenizerImpl

internal class StringIrTransformerTest {

    lateinit var tokenizer: Tokenizer
    lateinit var irFactory: StringIrFactory

    @BeforeEach
    fun setUp() {
        tokenizer = TokenizerImpl()
        irFactory = StringIrFactoryImpl()
    }

    @Test
    fun testBasic() {
        fun transformer(): StringIrTransformer = StringIrTransformFactory.createTransform(
                StringIrTransformFactory.BracketsType.Parentheses,
                surroundString = false,
                intoCenter = false
        )

        assertEquals("a(bc)d", prettyString(transformer(), "abcd"))

        assertEquals("a(b(c)d)e", prettyString(transformer(), "abcde"))
        assertEquals("", prettyString(transformer(), ""))
        assertEquals("a", prettyString(transformer(), "a"))
    }

    @Test
    fun testSurrounding() {
        fun transformer(): StringIrTransformer = StringIrTransformFactory.createTransform(
                StringIrTransformFactory.BracketsType.Parentheses,
                surroundString = true,
                intoCenter = false
        )

        assertEquals("(a(bc)d)", prettyString(transformer(), "abcd"))

        assertEquals("(a(b(c)d)e)", prettyString(transformer(), "abcde"))
        assertEquals("()", prettyString(transformer(), ""))
        assertEquals("(a)", prettyString(transformer(), "a"))
    }


    @Test
    fun testCenter() {
        fun transformer(): StringIrTransformer = StringIrTransformFactory.createTransform(
                StringIrTransformFactory.BracketsType.Parentheses,
                surroundString = false,
                intoCenter = true
        )

        assertEquals("a(b()c)d", prettyString(transformer(), "abcd"))

        assertEquals("a(b(c)d)e", prettyString(transformer(), "abcde"))
        assertEquals("()", prettyString(transformer(), ""))
        assertEquals("a", prettyString(transformer(), "a"))
    }

    @Test
    fun testCenterSurrounded() {
        fun transformer(): StringIrTransformer = StringIrTransformFactory.createTransform(
                StringIrTransformFactory.BracketsType.Parentheses,
                surroundString = true,
                intoCenter = true
        )

        assertEquals("(a(b()c)d)", prettyString(transformer(), "abcd"))

        assertEquals("(a(b(c)d)e)", prettyString(transformer(), "abcde"))
        assertEquals("(())", prettyString(transformer(), ""))
        assertEquals("(a)", prettyString(transformer(), "a"))
    }


    @Test
    fun testCircled() {
        fun transformer(): StringIrTransformer = StringIrTransformFactory.createTransform(
                StringIrTransformFactory.BracketsType.Circled(listOf('(' to ')', '{' to '}', '[' to ']')),
                surroundString = false,
                intoCenter = false
        )

        assertEquals("q(w{e[r(ty)u]i}o)p", prettyString(transformer(), "qwertyuiop"))

        assertEquals("a(bc)d", prettyString(transformer(), "abcd"))

        assertEquals("a(b{c}d)e", prettyString(transformer(), "abcde"))
        assertEquals("", prettyString(transformer(), ""))
        assertEquals("a", prettyString(transformer(), "a"))
    }


    @Test
    fun testFull() {
        fun transformer(): StringIrTransformer = StringIrTransformFactory.createTransform(
                StringIrTransformFactory.BracketsType.Circled(listOf('(' to ')', '{' to '}', '[' to ']')),
                surroundString = true,
                intoCenter = true
        )

        assertEquals("(q{w[e(r{t[]y}u)i]o}p)", prettyString(transformer(), "qwertyuiop"))

        assertEquals("(a{b[]c}d)", prettyString(transformer(), "abcd"))

        assertEquals("(a{b[c]d}e)", prettyString(transformer(), "abcde"))
        assertEquals("({})", prettyString(transformer(), ""))
        assertEquals("(a)", prettyString(transformer(), "a"))
    }

    private fun prettyString(transformer: StringIrTransformer, input: String): String {
        val tokens = tokenizer.tokenize(input)
        val ir = irFactory.build(tokens)
        return transformer.transform(ir).toPrettyString()
    }
}
